export default {
    users: state => state.users,
    isEdit: state => state.isEdit,
    user: state => state.user,
    id: state => state.id,
}