export default {
    setUsers(state, payload) {
        console.log(payload);
        state.users = payload;
    },

    setEditUser(state, payload) {
        state.user = payload;
        state.isEdit = true;
    },

    setEditId(state, payload) {
        state.id = payload;
    }
}